package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LightSaberTest {

    LightSaber lightSaber;

    @BeforeEach
    void setUp() {
        lightSaber = new LightSaber(123);
    }

    @Test
    void setChargeShouldSetChargeToParticularValue() {
        lightSaber.setCharge(50.0f);
        assertEquals(50.0f, lightSaber.getCharge());
    }

    @Test
    void setColorShouldSetColorToParticularValue() {
        lightSaber.setColor("blue");
        assertEquals("blue", lightSaber.getColor());
    }

    @Test
    void getChargeShouldReturnCharge() {
        lightSaber.setCharge(100.0f);
        assertEquals(100.0f, lightSaber.getCharge());
    }

    @Test
    void getJediSerialNumberShouldReturnJediSerialNumber() {
        assertEquals(123, lightSaber.getJediSerialNumber());
    }

    @Test
    void getColorShouldReturnColor() {
        lightSaber.setColor("green");
        assertEquals("green", lightSaber.getColor());
    }

    @Test
    void useShouldDecreaseChargeByEfficiencyDividedBy60TimesMinutes() {
        lightSaber.setCharge(100.0f);
        lightSaber.use(10.0f);
        assertEquals(98.333336f, lightSaber.getCharge());
    }

    @Test
    void getRemainingMinutesShouldReturnChargeDividedByEfficiencyTimes30() {
        lightSaber.setCharge(100.0f);
        assertEquals(300.0f, lightSaber.getRemainingMinutes());
    }

    @Test
    void rechargeShouldSetChargeTo100() {
        lightSaber.setCharge(50.0f);
        lightSaber.recharge();
        assertEquals(100.0f, lightSaber.getCharge());
    }
}